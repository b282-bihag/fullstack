import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link } from 'react-router-dom';
import { useContext } from 'react';
import UserContext from '../UserContext';

function AppNavbar() {
    // const [user] = useState(localStorage.getItem('email'));
    const { user } = useContext(UserContext);

    return (
        <Navbar expand="lg" className="bg-body-tertiary">
            <Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                    <Nav.Link as={Link} to="/">Home</Nav.Link>
                    <Nav.Link as={Link} to="/courses">Courses</Nav.Link>


                    {(user.id) ?
                        // Show Logout when logged in
                        <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                        :
                        // Show Login and Register when logged out
                        <>
                            <Nav.Link as={Link} to="/login">Login</Nav.Link>
                            <Nav.Link as={Link} to="/register">Register</Nav.Link>
                        </>
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}

export default AppNavbar;
