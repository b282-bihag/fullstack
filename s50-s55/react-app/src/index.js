import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>

    <App />

  </React.StrictMode>
);

// const name = 'Josh Perez';
// const element = <h1>Hello, {name}</h1>;

// function formatName(user) {
//   return user.firstName + ' ' + user.lastName;
// };

// const element = (
//   <h1>
//     Hello, {formatName({ firstName: 'Josh', lastName: 'Perez' })}!
//   </h1>
// );

// root.render(
//   element
// );

