function countLetter(letter, sentence) {
    let result = 0;

    if (typeof letter !== 'string' || letter.length !== 1) {
        return undefined;
    }

    const lowerCaseLetter = letter.toLowerCase();
    const lowerCaseSentence = sentence.toLowerCase();

    for (let i = 0; i < lowerCaseSentence.length; i++) {
        if (lowerCaseLetter === lowerCaseSentence[i]) {
            result++;
        }
    }

    return result;
}


function isIsogram(text) {
    const textLower = text.toLowerCase();
    for (let i = 0; i < textLower.length; i++) {
        for (let j = i + 1; j < textLower.length; j++) {
            if (textLower[i] === textLower[j]) {
                return false;
            }
        }
    }
    return true;
}


function purchase(age, price) {
    if (age < 13) {
        return undefined;
    }

    if (age >= 13 && age <= 21 || age >= 65) {
        const discountedPrice = price * 0.8;
        return discountedPrice.toFixed(0);
    }

    return price.toFixed(2);
}

function findHotCategories(items) {

    const categories = [];
    const uniqueCategories = new Set();

    for (let i = 0; i < items.length; i++) {
        if (items[i].stocks === 0) {
            const category = items[i].category;
            if (!uniqueCategories.has(category)) {
                uniqueCategories.add(category);
                categories.push(category);
            }
        }
    }

    return categories;
}


function findFlyingVoters(candidateA, candidateB) {

    const voters = [];
    for (let i = 0; i < candidateA.length; i++) {
        if (candidateB.includes(candidateA[i])) {
            voters.push(candidateA[i]);
        }
    }
    return voters;
}


// function countLetter(letter, sentence) {
//     let result = 0;

//     // Check first whether the letter is a single character.
//     // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
//     // If letter is invalid, return undefined.
//     if (letter.length > 1) {
//         return undefined;
//     }
//     for (let i = 0; i < sentence.length; i++) {
//         if (letter === sentence[i]) {
//             result++;
//         }
//     }
//     return result;
// }


// function isIsogram(text) {
//     // An isogram is a word where there are no repeating letters.
//     // The function should disregard text casing before doing anything else.
//     // If the function finds a repeating letter, return false. Otherwise, return true.
//     const textLower = text.toLowerCase();
//     for (let i = 0; i < textLower.length; i++) {
//         for (let j = i + 1; j < textLower.length; j++) {
//             if (textLower[i] === textLower[j]) {
//                 return false;
//             }
//         }
//     }
//     return true;
// }


// function purchase(age, price) {
//     // Return undefined for people aged below 13.
//     // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
//     // Return the rounded off price for people aged 22 to 64.
//     // The returned value should be a string.
//     if (age < 13) {
//         return undefined;
//     }
//     if (age >= 13 && age <= 21 || age >= 65) {
//         const discountedPrice = price * 0.8;
//         return discountedPrice.toFixed(0);
//     }
//     return price.toFixed(0).toString();
// }

// function findHotCategories(items) {
//     // Find categories that has no more stocks.
//     // The hot categories must be unique; no repeating categories.

//     // The passed items array from the test are the following:
//     // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
//     // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
//     // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
//     // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
//     // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

//     // The expected output after processing the items array is ['toiletries', 'gadgets'].
//     // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
//     const categories = [];
//     for (let i = 0; i < items.length; i++) {
//         if (items[i].stocks === 0) {
//             if (!categories.includes(items[i].category)) {
//                 categories.push(items[i].category);
//             }
//         }
//     }
//     return categories;
// }


module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};